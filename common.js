const common = module.exports = {

    isPeselValid: (v) => {
        if(v.length != 11) return false
        let weight = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3]
        let sum = 0
        let controlNumber = parseInt(v.substring(10, 11))
        for (let i = 0; i < weight.length; i++) {
            sum += (parseInt(v.substring(i, i + 1)) * weight[i])
        }
        sum = sum % 10
        if((10 - sum) % 10 === controlNumber) return true
        return false
      }
      
}