# pai2023

## Przygotowanie projektu do pracy nad nim
1. Sklonować projekt z https://gitlab.com/mariusz.jarocki/pai2023
1. W głównym katalogu projektu zainstalować zależności: `npm install`
1. Skopiować config-example.json do config.json i wyedytować ten ostatni
1. Uruchomić backend: `npm start`
1. Z katalogu frontend uruchomić serwer roboczy: `npm run serve`
1. Z przeglądarki połączyć się z https://localhost:8080

## Przygotowanie wersji produkcyjnej
1. Z katalogu frontend `npm run build`
1. Z przeglądarki połączyć się z https://localhost:8000 (port z konfiguracji)

## Zadania zaliczeniowe

### Zadanie podstawowe (na 3)

Dodaj nową kolekcję do bazy, projects. Kolekcja będzie przechowywać projekty, posiadające nazwę, datę utworzenia oraz typ (jeden z trzech, np. zewnętrzny/wewnętrzny/inny, nie wymagam kolekcji typów projektu). Stwórz API backendowe do jej obsługi, umożliwiające przeglądanie, dodawanie, zmienianie i usuwanie projektów (nazw, dat i typów). Oprogramuj widok frontendowy, umożliwiający wygodną obsługę tego API. API i widok mają być dostępne wyłącznie dla zalogowanych użytkowników.

### Zadanie rozszerzone (na 4)

Projekt z poprzedniego zadania ma być rozszerzony o identyfikator kierownika, wiążący każdy projekt z jednym obiektem z kolekcji users. API ma być odpowiednio rozszerzone, a w UI mają pojawić się elementy ułatwiające wybór kierownika dla projektu (select). Dodatkowo widok osób ma zostać rozbudowany o liczbę projektów, których osoba jest kierownikiem.

### Zadanie rozszerzone (na 5)

Model projektu z zadania na 4 ma zostać rozszerzony o members czyli tablicę identyfikatorów wykonawców (z kolekcji users). W UI ma pojawić się możliwość wygodnego wyboru, które osoby należą do members (select multiple lub podobne rozwiązanie). Do members nie może należeć kierownik projektu. W widoku osób podejrzeć można w ilu projektach dana osoba jest wymieniona w members projektu.

## Branch 'extra'

Został tu przepisana duża część kodu backendu, aby uczynić go zgodnym z najnowszymi wersjami Mongoose i AJV. Zalecam usunięcie katalogów node_modules i pliku package_lock.json oraz ponowny npm install.

Branch ten może być wykorzystany przez osoby aspirujące do oceny 4 i 5. W modelu danych (User.js) mamy dwa dodatkowe pola: aggregation i preparation. Pierwsze to tablica przekształceń wykonywanych podczas odczytu danych. Można w niej (dla Project.js) zakodować połączenie ($lookup) z inną kolekcją (np. User.js). Drugie to funkcja, która będzie wywołana przed utworzeniem/aktualizacją obiektu.

Przykładowe agregation dla kolekcji Project może wyglądać następująco:
```js
  [
    {
      $lookup: {
        from: 'users',
        localField: 'owner_id',
        foreignField: '_id',
        as: 'manager'
      }
    },
    { $unwind: { path: '$manager' } },
    { $project: { 'manager.password': false } }
  ]
```
preparation natomiast:
```js
  (project) => {
    let projectWihoutManager = {}
    Object.assign(projectWithoutManager, project)
    delete projectWithoutManager.manager
    return projectWithoutManager
  }
```