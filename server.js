const fs = require('fs')

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const ajv = require('ajv')
const expressSession = require('express-session')
const passport = require('passport')
const passportJson = require('passport-json')

const db = require('./db')
const auth = require('./auth')
const User = require('./User')

let config = {}
try {
    config = JSON.parse(fs.readFileSync('config.json'))
} catch(ex) {
    console.error('Configuration error:', ex.message)
    process.exit(0)
}

const app = express()

app.use(morgan('tiny'))
app.use(cors())
app.use(bodyParser.json())
// handle errors on body parser
app.use((err, req, res, nextTick) => {
    if(err) {
        res.status(400).json({ error: 'Invalid payload', message: err.message })
    } else {
        nextTick()
    }
})

// authorization middleware
app.use(expressSession({ secret: config.dbUrl, resave: false , saveUninitialized: true }))
app.use(passport.initialize())
app.use(passport.session())
passport.use(new passportJson.Strategy(auth.checkCredentials))
passport.serializeUser(auth.serialize)
passport.deserializeUser(auth.deserialize)

// authentication endpoints
app.get('/auth', auth.whoami)
app.post('/auth', passport.authenticate('json', { failWithError: true }), auth.login, auth.errorHandler)
app.delete('/auth', auth.logout)

app.use(express.static('./frontend/dist'))

app.get('/user', auth.checkIfInRole([ 0, 1 ]), db.read(User, { password: false }))
app.post('/user', auth.checkIfInRole([ 0 ]), db.create(User, { password: false }))
app.put('/user', auth.checkIfInRole([ 0 ]), db.update(User, { password: false }))
app.delete('/user', auth.checkIfInRole([ 0 ]), db.delete(User, { password: false }))

db.init(config, () => {


    ajvParser = new ajv()

    User.init(ajvParser)

    User.model.find({ roles: 0 }, (err, docs) => {
        if(!err && (!docs || !docs.length) && config.defaultAdmin ) {
            console.log('Creating default admin account', JSON.stringify(config.defaultAdmin))
            let admin = new User.model(config.defaultAdmin)
            admin.roles = [ 0 ]
            admin.save()
        }
        app.listen(config.port, () => {
            console.log('Backend listening on', config.port)
        })
    })
})