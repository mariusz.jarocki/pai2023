const mongoose = require('mongoose')

const User = require('./User')

const db = module.exports = {
    init: (config, nextTick) => {
        mongoose.set('strict', true)
        mongoose.set('strictQuery', false)
        mongoose.connect(config.dbUrl, (err) => {
            if(err) {
                console.error(err.message)
                process.exit(0)
            }
            console.log('Connected to database host', new URL(config.dbUrl).host)
            nextTick()
        })
    },

    create: (Entity, projection) => (req, res) => {
        let valid = Entity.validator.check(req.body)
        if(valid) {
            let entity = new Entity.model(req.body)
            entity.save((err) => {
                if(err) {
                    res.status(400).json({ message: 'Save error', error: err.message })
                    return
                }
                Entity.model.find({}, projection, (err, data) => {
                    res.json(data)
                })
            })    
        } else {
            let errorMessage = Entity.validator.check.errors.map(el => el.dataPath + ' ' + el.message).join(';')
            res.status(400).json({ message: 'Validation error on save', error: Entity.name + errorMessage })
        }
    },

    read: (Entity, projection) => (req, res) => {
        Entity.model.find({}, projection, (err, data) => {
            res.json(data)
        })
    },
    
    update: (Entity, projection) => (req, res) => {
        let valid = Entity.validator.check(req.body)
        if(valid) {
            Entity.model.updateOne({ _id: req.body._id }, req.body, (err) => {
                if(err) {
                    res.status(400).json({ message: 'Update error', error: err.message })
                    return
                }
                Entity.model.find({}, projection, (err, data) => {
                    res.json(data)
                })
            })    
        } else {
            let errorMessage = Entity.validator.check.errors.map(el => el.dataPath + ' ' + el.message).join(';')
            res.status(400).json({ message: 'Validation error on update', error: Entity.name + errorMessage })
        }
    },

    delete: (Entity, projection) => (req, res) => {
        let _id = req.query && req.query._id
        if(_id) {
            Entity.model.deleteOne({ _id }, (err) => {
                if(err) {
                    res.status(400).json({ message: 'Delete error', error: err.message })
                    return
                }
                Entity.model.find({}, projection, (err, data) => {
                    res.json(data)
                })
            })    
        } else {
            res.status(400).json({ message: 'Error on delete', error: Entity.name + ' wrong _id' })
        }
    }
}