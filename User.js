const mongoose = require('mongoose')
const uuid = require('uuid')

const common = require('./common')

const User = module.exports = {

    name: 'User',
    collection: 'users',

    init: (ajvParser) => {

        // checking a user object consistency
        ajvParser.addKeyword('userChecker', {
            type: 'object',
            schemaType: 'boolean',
            compile: () => (user) => {
                if(!user.pesel || !common.isPeselValid(user.pesel)) {    
                    return false
                }
                return true
            }
        })
        
        // checking a user types, patterns, formats and ranges
        User.validator = {
            type: 'object',
            properties: {
                _id: { type: 'string', format: 'uuid' },
                firstName: { type: 'string', pattern: '^[A-Z]' },
                lastName: { type: 'string', pattern: '^[A-Z]' },
                email: { type: 'string', format: 'email' },
                pesel: { type: 'string', pattern: '^[0-9]{11}$' },
                roles: { type: 'array', items: { type: 'integer' } }
            },
            required: [ 'firstName', 'lastName', 'email', 'pesel', 'roles' ],
            additionalProperties: false,
            userChecker: true
        }
        
        User.validator.check = ajvParser.compile(User.validator)

        // create mongoose model
        User.model = mongoose.model(User.name, new mongoose.Schema({
            _id: { type: String, default: uuid.v4 },
            firstName: String,
            lastName: String,
            email: String,
            password: String,
            pesel: String,
            roles: [ Number ]
        }, { versionKey: false }), User.collection)                
    }
}