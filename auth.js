const db = require('./db')
const User = require('./User')

const auth = module.exports = {
    checkCredentials: (email, password, nextTick) => {
        User.model.findOne({ email, password }, (err, user) => {
            return nextTick(null, err || !user ? false : user)
        })
    },
    checkIfInRole: (roleNums) => (req, res, nextTick) => {
        let intersection = []
        if(roleNums == null) {
            intersection.push(-1)
        } else {
            roleNums.forEach((roleNum) => {
                if(req.user && req.user.roles && req.user.roles.includes(roleNum)) {
                    intersection.push(roleNum)
                }
            })
        }
        if(!req.isAuthenticated()) {
            res.status(401).json({ message: 'Authorization failure', error: 'Not authorized' })
        } else if(intersection.length > 0) {
            return nextTick()
        } else {
            res.status(403).json({ message: 'Authorization failure', error: 'Permission denied' })
        }
    },
    serialize: (user, nextTick) => {
        nextTick(null, user._id)
    },
    deserialize: (_id, nextTick) => {
        User.model.findOne({ _id }, (err, user) => {
            if(err || !user) {
                nextTick('No such user', null)
            } else {
                nextTick(null, user)
            }
        })
    },
    login: (req, res) => {
        auth.whoami(req, res)
    },
    logout: (req, res) => {
        req.logout(() => { res.json({}) })
    },
    whoami: (req, res) => {
        req.session.roles = req.user ? req.user.roles : []
        req.session.save()
        let data = {}
        if(req.user) {
            data.email = req.user.email
            data.name = req.user.firstName + ' ' + req.user.lastName
            data.roles = req.user.roles
            data.sessionid = req.session.id
        }
        res.json(data)
    },
    errorHandler: (err, req, res, nextTick) => {
        res.json({ message: 'Authorization failure', error: err.message })
    }
}